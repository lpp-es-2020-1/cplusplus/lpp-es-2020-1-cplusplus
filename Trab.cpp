#include<iostream>
#include<time.h>

using namespace std;

int main() {

    int venda=0,vende,soma=0,i;
    float lucro = 0.0,vendedor[3],vendedor2[3];
    clock_t Ticks[2];
    Ticks[0] = clock();


    for (i = 0; i < 3; i++){
        vendedor[i]=0;
        vendedor2[i]=0;
    }



    while(1){
        cin >> venda >> vende;

        if(venda==9999){
            ;break;
        }

        lucro = venda*0.25;

        if(lucro>2200.00)
            vendedor[vende-1]+=lucro*0.01;
        else if(lucro>1500.01)
            vendedor[vende-1]+=lucro*0.02;
        else if(lucro>500.01)
            vendedor[vende-1]+=lucro*0.03;
        else if(lucro>150.01)
            vendedor[vende-1]+=lucro*0.04;
        else
            vendedor[vende-1]+=lucro*0.05;

        vendedor2[vende-1]+=venda;
        soma = soma+venda;

    }

    cout << "Venda Total: " << soma << "\n";

    for (i = 0; i < 3; i++)
    {
        cout << "Venda total do Vendedor " << i+1 << ": " << vendedor2[i] << ", Comissão: " << vendedor[i] << "\n";
    }

    cout << "Lucro liquido da loja: " << soma*0.25 - (vendedor[0]+vendedor[1]+vendedor[2]) << "\n";

    Ticks[1] = clock();
    double Tempo = (Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC;
    printf("Tempo gasto: %g ms.", Tempo);

    return 0;
}